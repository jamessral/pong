CC = gcc
FLAGS = -std=c11 -Wall -Wpedantic -Werror
INCLUDE = -I/usr/include
LIB_DIR = -L/usr/lib
LIBS = -lSDL2 -lSDL2_image -lSDL2_ttf -lSDL2_mixer
EXE = Pong
SRC = src/main.c


all:
	$(CC) $(SRC) -o $(EXE) $(FLAGS) $(INCLUDE) $(LIB_DIR) $(LIBS)

src/*.o src/*.c:
	$(CC) $(SRC) -o $(EXE) $(FLAGS) $(INCLUDE) $(LIB_DIR) $(LIBS)


clean:
	rm $(EXE)


play:
	./$(EXE)
