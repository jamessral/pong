#pragma once
#include <stdbool.h>
#include <SDL2/SDL.h>

bool is_key_pressed(SDL_Event event, unsigned int keycode);
