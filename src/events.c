#include "events.h"

bool is_key_pressed(SDL_Event event, unsigned int keycode) {
	return event.key.keysym.sym == keycode;
}
