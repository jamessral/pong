#include <stdbool.h>
#include <SDL2/SDL.h>
#include "events.c"

int main(int argc, char** argv) {
	if (SDL_Init(SDL_INIT_VIDEO) != 0){
		printf("SDL_Init Error: ");
		printf("%s\n", SDL_GetError());
		return 1;
	}

	SDL_Window* window = SDL_CreateWindow("Pong", 10, 10, 800, 600, SDL_WINDOW_SHOWN);
	if (window == NULL) {
		printf("SDL_CreateWindow Error: ");
		printf("%s\n", SDL_GetError());
		return 1;
	}

	SDL_Renderer* renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
	if (renderer == NULL) {
		printf("SDL_CreateRenderer Error: ");
		printf("%s\n", SDL_GetError());
		return 1;
	}

	bool should_quit = false;
	SDL_Event event;

	while (!should_quit) {
		SDL_RenderClear(renderer);
		while (SDL_PollEvent(&event) != 0) {
			if (event.type == SDL_QUIT) {
				should_quit = true;
			} else if (event.type == SDL_KEYDOWN) {
				if (is_key_pressed(event, SDLK_ESCAPE)) {
					should_quit = true;
				}
			}
		}
	}

	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);
	SDL_Quit();

	return 0;
}
